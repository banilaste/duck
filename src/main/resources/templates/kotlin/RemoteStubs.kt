package io.github.banilaste.duck.generated

import io.github.banilaste.duck.*
import io.github.banilaste.duck.proxy.Proxy
import io.github.banilaste.duck.marshall.typeOf

{% for name, class in remote.items() %}
@RemoteStub
open class {{ name }}Stub(id: RemoteObjectIdentifier) : {{ name }}(id) {
	{%
		for field in class.fields
	%}
	override var {{ field.name }}: {{ field | type }}
		get() = Proxy.get(this, {{ field.id }}, typeOf({{ field | type(false) }}::class, {{ field.dataType.arrayDepth }}, {{ "true" if field.dataType.nullable else "false" }}))
		{{ "protected" if field.readonly }} set(value) = Proxy.set(this, {{ field.id }}, value)
	{%
		endfor
	%}
	{%
		for func in class.functions
	%}
	override fun {{ func.name }}({% 
		for input in func.inputs 
			%}{{ ", " if not loop.first }}{{ input.name }}: {{ input | type }}{%
		endfor 
	%}){% if func.return %}: {{ func.return | type }}{% endif %} {
		{{ "return" if func.return }} Proxy.call<{{
			func.return | type if func.return else "Unit"
		}}>(this, {{ func.id }}, arrayOf({% 
				for input in func.inputs 
					%}{{ ", " if not loop.first }}{{ input.name }}{%
				endfor 
			%}), typeOf({{
				(func.return if func.return else "Unit") | type(true, false)
			}}::class, {{ func.return.arrayDepth if func.return else "0" }}, {{ "true" if func.return.nullable else "false" }}))!!
	}
	{%
		endfor
	%}
}

{% endfor %}