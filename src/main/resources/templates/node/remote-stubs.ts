import { RemoteInterface, RemoteObject, RemoteObjectIdentifier,
	RemoteStub, Proxy, ByteBuffer, typeOf, RawType } from "duck-node";
import {
	{% for name, class in remote.items() %}{{ name }}, {% endfor %}
	{% for name, class in data.items() %}{{ name }}, {% endfor %}
} from "./remote-objects";

{% for name, class in remote.items() %}
@RemoteStub
export class {{ name }}Stub extends {{ name }} {
	constructor(id: RemoteObjectIdentifier) {
		super(id)
	}

	{% for field in class.fields %}
		get {{ field.name }}: {{ field | type }} {
			return Proxy.get(this, {{ field.id }}, typeOf(
				{{ field | type(false) }},
				{{ field.dataType.arrayDepth }},
				{{ "true" if field.dataType.nullable else "false" }}
			))
		}

		{{ "protected" if field.readonly }} set {{ field.name }}(value: {{ field | type }}) {
			Proxy.set(this, {{ field.id }}, value)
		}
	{% endfor %}
	
	{% for func in class.functions %}
		{{ func.name }}(
			{% for input in func.inputs %}
				{{ ", " if not loop.first }}
				{{ input.name }}: {{ input | type }}
			{% endfor %}
		): Promise<{% if func.return %}{{ func.return | type }}{% else %}void{% endif %}> {
			return Proxy.call<{{
				func.return | type if func.return else "void"
			}}>(this, {{ func.id }}, [
					{% for input in func.inputs %}
						{{ ", " if not loop.first }}
						{{ input.name }}
					{% endfor %}
				], typeOf(
					{{ (func.return | type(false)) if func.return else "RawType.UNDEFINED" }},
					{{ func.return.arrayDepth if func.return else "0" }},
					{{ "true" if func.return.nullable else "false" }}
				)
			)
		}
	{% endfor %}
}

{% endfor %}