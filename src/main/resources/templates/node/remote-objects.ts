import { RemoteInterface, RemoteObject, RemoteObjectIdentifier, ByteBuffer, typeOf, RawType, DataClass } from "duck-node";

/**
 * Data classes
 */
{% for name, class in data.items() %}
@DataClass([
	{% for field in class.fields %}
{{ ", " if not loop.first }}
{
	id: {{field.id}},
	type: typeOf(
		{{ field.dataType | type(false) }},
	{{ field.dataType.arrayDepth }},
	{{ "true" if field.dataType.nullable else "false" }}
),
	name: "{{field.name}}"
}
{% endfor %}
])
export class {{ name }} {
	constructor(
		{% for field in class.fields %}
	{{ ", " if not loop.first }}
	public {{ field.name }}: {{ field | type }}
		{% endfor %}
	) {}
}
{% endfor %}


/**
 * Remote objects
 */
{% for name, class in remote.items() %}
	@RemoteInterface({
		{% for field in class.fields %}
		"{{ field.id }}": {
			method: "{{ field.name }}",
			numberType: {{ field.dataType | type(false) }},
			argumentTypes: [ typeOf(
				{{ field.dataType | type(false) }},
				{{ field.dataType.arrayDepth }},
				{{ "true" if field.dataType.nullable else "false" }}
			) ]
		},
		{% endfor %}
		{% for func in class.functions %}
		"{{ func.id }}": {
			method: "{{ func.name }}",
			{% if func.return %}
				numberType: {{ func.return | type(false) }},
			{% endif %}
			argumentTypes: [
				{% for input in func.inputs %}
					{{ ", " if not loop.first }}
					typeOf(
						{{ input.dataType | type(false) }},
						{{ input.dataType.arrayDepth }},
						{{ "true" if input.dataType.nullable else "false" }}
					)
				{% endfor %}
			],
			noCaching: {{ "true" if func.noCaching else "false" }}
		},
		{% endfor %}
	})
	export abstract class {{ name }} extends RemoteObject {
		constructor(id: RemoteObjectIdentifier | false = false) {
			super(id ? id : RemoteObject.localIdentifier())
		}

		{% for field in class.fields %}
		public {{ "readonly" if field.readonly }} {{ field.name }}: {{ field | type }}
		{% endfor %}

		{% for func in class.functions %}
			abstract {{ func.name }}(
				{% for input in func.inputs %}
					{{ ", " if not loop.first }}
					{{ input.name }}: {{ input | type }}
				{% endfor %}
			): Promise<
				{% if func.return %}
					{{ func.return | type }}
				{% else %}
					void
				{% endif %}
			>;
		{% endfor %}
	}
{% endfor %}