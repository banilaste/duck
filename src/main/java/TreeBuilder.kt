package io.github.banilaste.duck.generator

/**
 * Builder for the data structure used in the templates
 */
class TreeBuilder(val tokens: MutableList<Token>) {
    private val error = Exception("issue during tree valruction, please check your duck file")

    private val remoteClasses = HashMap<String, Class>()
    private val dataClasses = HashMap<String, DataClass>()

    private var procedureIndex = 0

    private fun shiftText(): String {
        if (this.tokens.first().type === TokenType.TEXT) {
            val content = this.tokens.removeAt(0).content

            if (content != null) {
                return content
            }
        }

        throw unexpected(tokens.first())
    }

    private fun nextIs(vararg types: TokenType): Boolean {
        return types.any { this.tokens.first().type === it }
    }

    private fun handleClass() {
        val classObject =
            Class(this.shiftText(), ArrayList(), ArrayList())

        if (classObject.name in this.remoteClasses || classObject.name in this.dataClasses) {
            throw Exception("multiple definitions of ${classObject.name}")
        }

        while (!nextIs(TokenType.CLASS, TokenType.DATA_CLASS, TokenType.EOF)) {
            if (this.nextIs(TokenType.FUNCTION)) {
                this.tokens.removeAt(0)
                this.handleFunction(classObject)
            } else {
                handleField(classObject)
            }
        }

        this.remoteClasses[classObject.name] = classObject
    }

    private fun handleDataClass() {
        val classObject = DataClass(
            this.shiftText(),
            ArrayList()
        )

        if (classObject.name in this.remoteClasses || classObject.name in this.dataClasses) {
            throw Exception("multiple definitions of ${classObject.name}")
        }

        while (!nextIs(TokenType.CLASS, TokenType.DATA_CLASS, TokenType.EOF)) {
            handleField(classObject)
        }

        this.dataClasses[classObject.name] = classObject
    }

    private fun handleField(classObject: DataClass) {
        val readonly = this.nextIs(TokenType.READONLY)

        if (readonly) {
            tokens.removeAt(0)
        }

        classObject.fields.add(
            Field(
                this.procedureIndex++,
                handleType(),
                shiftText(),
                readonly
            )
        )
    }

    private fun handleType(): DataType {
        var arrayDepth = 0
        var nullable = false

        while (!this.nextIs(TokenType.TEXT)) {
            val token = tokens.removeAt(0)

            when (token.type) {
                TokenType.ARRAY -> arrayDepth++
                TokenType.NULLABLE -> nullable = true
                else -> throw unexpected(token)
            }
        }

        return DataType(
            shiftText(),
            arrayDepth,
            nullable
        )
    }

    private fun handleFunction(classObject: Class) {
        val noCache = nextIs(TokenType.NO_CACHE)
        if (noCache) {
            this.tokens.removeAt(0)
        }

        val aFunction = ClassFunction(
            this.procedureIndex++,
            shiftText(),
            noCache,
            ArrayList()
        )

        // Add input parameters
        while (this.nextIs(TokenType.TEXT)) {
            aFunction.inputs.add(
                Argument(
                    handleType(),
                    this.shiftText()
                )
            )
        }

        // Return type
        if (this.nextIs(TokenType.RETURN)) {
            this.tokens.removeAt(0)
            aFunction.`return` = handleType()
        }

        classObject.functions.add(aFunction)
    }

    private fun unexpected(token: Token): Exception {
        return Exception("unexpected token ${token.type}")
    }

    fun build(): Result {
        while (!this.nextIs(TokenType.EOF)) {
            val token = this.tokens.removeAt(0)
            when (token.type) {
                TokenType.CLASS -> this.handleClass()
                TokenType.DATA_CLASS -> this.handleDataClass()
                else -> throw this.unexpected(token)
            }
        }

        return Result(this.remoteClasses, this.dataClasses)
    }
}